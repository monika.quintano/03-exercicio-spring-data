package com.itau.funcionarios.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.funcionarios.models.Funcionario;

public interface FuncionarioRepository extends CrudRepository<Funcionario, Integer> {

}
