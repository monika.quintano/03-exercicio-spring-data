package com.itau.funcionarios.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itau.funcionarios.models.Funcionario;
import com.itau.funcionarios.services.FuncionarioService;

@RestController
@RequestMapping("/funcionario")
public class FuncionarioController {
	
	@Autowired
	FuncionarioService funcionarioService;
	
	@GetMapping
	public Iterable<Funcionario> listarLocalizacoes(){
		return funcionarioService.buscarTodos();
	}
	
	@PostMapping
	public void inserirFuncionario(@RequestBody Funcionario funcionario) {
		funcionarioService.inserir(funcionario);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity substituirFuncionario(@PathVariable int id, @RequestBody Funcionario funcionario) {
		boolean resultado = funcionarioService.substituir(id, funcionario);
		
		if(!resultado) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(funcionario);
	}
	
	@PatchMapping("/{id}")
	public ResponseEntity atualizarFuncionario(@PathVariable int id, @RequestBody Funcionario funcionario) {
		boolean resultado = funcionarioService.atualizar(id, funcionario);
		
		if(!resultado) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(funcionario);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity deletarFuncionario(@PathVariable int id) {
		boolean resultado = funcionarioService.remover(id);
		
		if(!resultado) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok().build();
	} 
}
