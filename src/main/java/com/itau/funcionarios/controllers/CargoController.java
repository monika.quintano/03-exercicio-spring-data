package com.itau.funcionarios.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itau.funcionarios.models.Cargo;
import com.itau.funcionarios.repositories.CargoRepository;

@RestController
@RequestMapping("/cargo")
public class CargoController {
	
	@Autowired
	CargoRepository cargoRepository;
	
	@GetMapping
	public Iterable<Cargo> listarLocalizacoes(){
		return cargoRepository.findAll();
	}
	
	@PostMapping
	public void inserirCargo(@RequestBody Cargo cargo) {
		cargoRepository.save(cargo);
	}
}
